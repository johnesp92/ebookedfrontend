import { createApp } from 'vue'
import App from './App.vue'
import "@fortawesome/fontawesome-free/css/all.css"
import "primevue/sidebar"
import { createHttpLink } from '@apollo/client/core';
import { setContext } from 'apollo-link-context';
import { createApolloProvider } from '@vue/apollo-option';
import { ApolloClient } from '@apollo/client';
import { InMemoryCache } from '@apollo/client';
//Crear aplicación
const app = createApp(App);
//Montar aplicación

const httpLink = createHttpLink({
    uri:"https://ebooked-api-gateway.herokuapp.com/"
});

const authLink = setContext((_, {headers}) =>{
    return{
        headers:{
            ...headers,
            'Authorization': "token"
        }
    }
});

const apolloClient = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
});

const apolloProvider = new createApolloProvider({
    defaultClient: apolloClient
})

app.use(apolloProvider);

app.mount('#app');
